from django.db.models import (
    Model,
    CharField,
    IntegerField,
    ForeignKey,
    DateField,
    CASCADE,
)


SCORES = [(i, n) for i, n in enumerate(("неуд", "удвл", "хор", "отл"))]


class Student(Model):
    firstname = CharField('имя', max_length=50)
    middlename = CharField('отчество', max_length=50, null=False, default='')
    lastname = CharField('фамилия', max_length=100)
    birthdate = DateField('дата рождения')

    class Meta:
        verbose_name = 'студент'
        verbose_name_plural = 'студенты'

    @property
    def fullname(self) -> str:
        name = f'{self.lastname} {self.firstname}'
        if self.middlename != '':
            name = f'{name} {self.middlename}'
        return name.lower().capitalize()
    
    @property
    def shortname(self) -> str:
        name = f'{self.lastname.lower().capitalize()} {self.firstname[0].upper()}.'
        if self.middlename != '':
            name = f'{name}{self.middlename[0].upper()}.'
        return name

    def __repr__(self) -> str:
        return f'<Student {self.id}:"{self.shortname}">'

    def __str__(self) -> str:
        return self.shortname



class Course(Model):
    caption = CharField('название', max_length=50)

    class Meta:
        verbose_name = 'предмет'
        verbose_name_plural = 'предметы'

    def __repr__(self) -> str:
        return f'<Course {self.id}:"{self.caption}">'

    def __str__(self) -> str:
        return self.caption


class ScoreRecord(Model):
    student = ForeignKey(Student, on_delete=CASCADE, verbose_name='студент')
    course = ForeignKey(Course, on_delete=CASCADE, verbose_name='предмет')
    score = IntegerField('оценка', choices=SCORES, null=False)

    class Meta:
        verbose_name = 'запись ведомости'
        verbose_name_plural = 'записи ведомости'

    def __repr__(self) -> str:
        return f'<ScoreRecord {self.id}:"{self.course.caption}":"{self.student.shortname}">'

    def __str__(self) -> str:
        return f'{self.course.caption} - {self.student.shortname}'
