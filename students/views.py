from urllib.parse import parse_qsl
from django.shortcuts import render
from rest_framework import viewsets, generics
from .serializers import (
    ScoreRecordHyperlinkedSerializer, 
    ScoreRecordWithCourseSerializer, 
    StudentSerializer, 
    CourseSerializer,
)
from .models import Course, Student, ScoreRecord
from datetime import date


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all().order_by('id')
    serializer_class = StudentSerializer


class CourseViewSet(viewsets.ModelViewSet):
    queryset = Course.objects.all().order_by('caption')
    serializer_class = CourseSerializer


class ScoreRecordViewSet(viewsets.ModelViewSet):
    queryset = ScoreRecord.objects.all().order_by('course')
    serializer_class = ScoreRecordHyperlinkedSerializer


class ScoreRecordWithCourseViewSet(generics.GenericAPIView):
    queryset = ScoreRecord.objects.all().order_by('course')
    serializer_class = ScoreRecordWithCourseSerializer