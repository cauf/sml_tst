from django.db.models import fields
from rest_framework import serializers
from rest_framework.serializers import (
    ModelSerializer,
    ListSerializer,
    Serializer,
    HyperlinkedModelSerializer,
)
from .models import (
    Student,
    Course,
    ScoreRecord,
)


class CourseSerializer(ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'


class ScoreRecordWithCourseSerializer(ModelSerializer):
    course = CourseSerializer(read_only=True)
    class Meta:
        model = ScoreRecord
        fields = ['score', 'course']


class StudentSerializer(ModelSerializer):
    scores = ScoreRecordWithCourseSerializer(many=True)
    class Meta:
        model = Student
        fields = ['firstname', 'middlename', 'lastname', 'birthdate', 'scores']


class ScoreRecordHyperlinkedSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = ScoreRecord
        fields = '__all__'