# Generated by Django 3.1.5 on 2021-01-20 09:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0005_auto_20210120_1222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scorerecord',
            name='score',
            field=models.IntegerField(choices=[(0, 'неуд'), (1, 'удвл'), (2, 'хор'), (3, 'отл')], verbose_name='оценка'),
        ),
    ]
