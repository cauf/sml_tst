from django.contrib import admin
from django.contrib.admin import (
    ModelAdmin,
    register,
    TabularInline,
)
from .models import (
    Student,
    Course,
    ScoreRecord,
)


class ScoreRecordTabular(TabularInline):
    model = ScoreRecord
    extra = 1


@register(Student)
class StudentAdmin(ModelAdmin):
    inlines = [
        ScoreRecordTabular, 
    ]


@register(Course)
class CourseAdmin(ModelAdmin):
    inlines = [
        ScoreRecordTabular, 
    ]


