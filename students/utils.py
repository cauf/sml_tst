from random import randint
from datetime import datetime

def get_birthday():
    month = randint(1,12)
    max_day = 28 if month == 2 else 30
    day = randint(1,max_day)
    year = 2000 + randint(-5,5)
    return datetime(year, month, day)
